﻿using Projects.Modelling.DTOs;

namespace Projects.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
