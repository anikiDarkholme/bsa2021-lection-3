﻿using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;

namespace Projects.DataAccess.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
