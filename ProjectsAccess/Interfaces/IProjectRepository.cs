﻿using Projects.Modelling.DTOs;

namespace Projects.DataAccess.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
    }
}
